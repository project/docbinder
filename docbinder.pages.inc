<?php
/**
 * @file
 * DocBinder pages.
 */

/**
 * DocBinder Page showing all currently collected documents.
 */
function docbinder_docbinder() {
  drupal_set_title(t('Your @name', array('@name' => variable_get('docbinder_name', 'docbinder'))));
  if (empty($_SESSION['docbinder']['files'])) {
    $content = variable_get('docbinder_empty_message', t('Sorry, you have no files queued for download.'));
  }
  else {
    $files = $_SESSION['docbinder']['files'];
    $content = '';
    if (!empty($files)) {
      $options = array(
        'html' => FALSE,
        'query' => array('destination' => current_path()),
        'attributes' => array('class' => array('docbinder-docbinder')),
      );
      $rows = $file_data = array();
      $link_text = t('Remove from cart');
      foreach ($files as $fid => $file) {
        $item = file_load($fid);
        $link_options = $options;
        $link_options['attributes']['id'] = 'docbinder-id-' . $item->fid;
        $path = url(DOCBINDER_PATH_REMOVE . '/' . $item->fid);
        $mime = check_plain($item->filemime);
        $icon_url = file_icon_url((object) $item);
        $icon = '<img class="file-icon" alt="" title="' . $mime . '" src="' . $icon_url . '" /> ';

        $rows[$fid] = array(
          'data' => array(
            'icon' => $icon,
            'filename' => $item->filename,
            'size' => format_size($item->filesize),
            'remove_link' => l($link_text, $path, $link_options),
          ),
          'id' => 'docbinder-docbinder-fid-' . $item->fid,
          'class' => array(
            'docbinder-docbinder-row',
          ),
        );
        $file_data[$fid] = $item;
      }
      $content = theme(
        'docbinder_page_downloads_table',
        array(
          'rows' => $rows,
          'attributes' => array(
            'class' => array(
              'docbinder-docbinder-table',
            ),
          ),
          'file_data' => $file_data,
        )
      );
      $options = array();
      $options['attributes']['class'][] = 'button';
      $content .= '<p>' .
        l(check_plain(variable_get('docbinder_download_button_text', t('Download collection'))), DOCBINDER_PATH_DOWNLOAD, $options) . ' ' .
        l(check_plain(variable_get('docbinder_empty_button_text', t('Empty cart'))), DOCBINDER_PATH_EMPTY, $options) .
        '</p>';
    }
  }
  return $content;
}
