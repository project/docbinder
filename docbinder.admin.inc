<?php
/**
 * @file
 * DocBinder admin pages.
 */

/**
 * Admin settings form.
 */
function docbinder_settings_form() {
  $methods = _docbinder_get_available_methods();
  foreach ($methods as $k => $v) {
    $docbinder_zip_method_options[$k] = $v['title'];
  }
  $form['docbinder_zip_method'] = array(
    '#title' => t('Zip Method'),
    '#type' => 'select',
    '#default_value' => variable_get('docbinder_zip_method', ''),
    '#options' => $docbinder_zip_method_options,
    '#description' => t('Method used to generate bundle files.'),
  );

  $form['docbinder_field_formatting'] = array(
    '#type' => 'fieldset',
    '#title' => t('Field formatting settings'),
    '#collapsible' => TRUE,
  );

  $form['docbinder_field_formatting']['docbinder_remove_hotlink'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove direct links to file.'),
    '#default_value' => variable_get('docbinder_remove_hotlink', 0),
    '#description' => t('Whether or not hotlinks to files (for direct download) should stay or be removed.'),
  );

  $form['docbinder_field_formatting']['docbinder_node_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node types'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Only selected node types will use the DocBinder field formatters. If none is selected, all node types will be affected.'),
  );

  $node_types = node_type_get_types();
  $docbinder_node_types_options = array();
  foreach ($node_types as $machine_name => $node_type) {
    $docbinder_node_types_options[$machine_name] = $node_type->name;
  }
  $form['docbinder_field_formatting']['docbinder_node_types']['docbinder_node_types_active'] = array(
    '#type' => 'checkboxes',
    '#options' => $docbinder_node_types_options,
    '#default_value' => variable_get('docbinder_node_types_active', array()),
  );

  $form['docbinder_indicator'] = array(
    '#title' => t('File status indicator'),
    '#type' => 'select',
    '#default_value' => variable_get('docbinder_indicator', 'plain'),
    '#options' => array('plain' => t('Plain text'), 'fafa' => t('fa fa-icon')),
    '#description' => t('Choose a way to indicator whether or not a file has been added to download collection.'),
  );

  $form['docbinder_animate'] = array(
    '#title' => t('Animation'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('docbinder_animate', 1),
    '#description' => t('Check this option if a small animation should be played when adding a file to download collection.'),
  );

  $form['docbinder_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => variable_get('docbinder_name', 'DocBinder'),
    '#description' => t('Enter a name that should be used in messages and user interface.'),
  );
  $form['docbinder_download_button_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Download button text'),
    '#default_value' => variable_get('docbinder_download_button_text', t('Download collection')),
    '#description' => t('Enter the text of the button to download collection.'),
  );
  $form['docbinder_empty_button_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Empty button text'),
    '#default_value' => variable_get('docbinder_empty_button_text', t('Empty cart')),
    '#description' => t('Enter the text of the button to delete all items from collection.'),
  );
  $form['docbinder_empty_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Empty binder message'),
    '#default_value' => variable_get('docbinder_empty_message', t('Sorry, you have no files queued for download.')),
    '#description' => t('Enter a message to display when there are no items in collection.'),
  );
  $form['docbinder_download_page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title for the download confirmation page'),
    '#default_value' => variable_get('docbinder_download_page_title', t('Thank you for downloading files')),
    '#description' => t('Enter a message for the download confirmation page.'),
  );
  $content = variable_get('docbinder_download_page_content', array(
    'value' => t('Your files should start to download shortly.  If they don\'t, <a href="!download_link">click here</a> to initiate the download.'),
    'format' => NULL,
  ));
  $form['docbinder_download_page_content'] = array(
    '#type' => 'text_format',
    '#title' => t('Content for the download confirmation page'),
    '#default_value' => $content['value'],
    '#description' => t('What content would you like for the download confirmation page? '
      . 'You can use the token <em>!download_link</em> as the quote-enclosed href in '
      . 'an anchor (&lt;a&gt;) tag to specify the link to click on to initiate '
      . 'the download if for some reason it doesn\'t start automatically). For example:<br><br>'
      . ' &nbsp; &nbsp; If the download doesn\'t start automatically, &lt;a href="!download_link"&gt;click here&lt;/a&gt;.'),
    '#format' => $content['format'],
  );

  return system_settings_form($form);
}
