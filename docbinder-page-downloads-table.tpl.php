<?php
/*
 * Template file for the table of files in the download cart.
 *
 * Variables:
 * - $header - array of table headers.
 * - $rows - array of table rows, keyed on fid.
 * - $attributes - rendered attributes (does not include classes).
 * - $attributes_array - array of table attributes.
 * - $classes - rendered classes for the table.
 * - $classes_array - the array of classes for the table.
 * - $file_data - additional file data, keyed on fid.
 */
?>
<div class="docbinder-downloads-wrapper">
  <?php
print theme(
  'table',
  array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array(
      'classes' => $classes_array,
    ),
  )
);
?>
</div>
